import java.util.Scanner;

class Imc {
    public static void main(String[] args) {

        System.out.println("--CALCULO DO IMC--");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Por Favor digite seu Peso: ");
        Double peso = scanner.nextDouble();
        System.out.println();

        System.out.print("Por Favor digite sua Altura: ");
        Double altura = scanner.nextDouble();
        System.out.println();

        Double calculo = peso / altura;

        System.out.print("Resultado do IMC é : " + calculo);

    }

}
